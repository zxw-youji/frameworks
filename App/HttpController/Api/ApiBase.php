<?php
namespace App\HttpController\Api;

use App\common\constants\ErrorCode;
use App\common\service\token\UserAccessTokenService;
use App\HttpController\BaseController;

abstract class ApiBase extends BaseController
{

    protected $isAuth = true; //默认开启验证

    protected $uid; //当前登录用户id

    //忽略登录认证的action(当控制器设为需登录认证时，可单独针对个别的action忽略登录认证)
    protected $ignoreAuthAction = [];

    protected $accessToken; //当前用户登录token


    protected function onRequest(?string $action): ?bool
    {

       $this->checkAuth();
       return true;
    }

    /**
     * 检查登录态
     */
    protected function checkAuth()
    {
        $this->accessToken = (string)$this->request()->getRequestParam('Access-Token');

        $service = new UserAccessTokenService();

        $this->uid = $service->checkAccessToken($this->accessToken);

        if (!$this->uid) {
            if ($this->isAuth) {
                if (!in_array($this->getAction(), $this->ignoreAuthAction)) {
                    $this->_error(ErrorCode::NOT_AUTH,'登录过期',['list'=>$this->ignoreAuthAction,'action'=>$this->getAction()]);
                    return true;
                }
            }
        }
        return true;
    }


    protected function getAction()
    {
        static $action;
        if (!isset($action)) {
            $uri = $this->request()->getUri()->getPath();
            $arr = array_values(array_filter(explode('/', $uri)));
            $action = $arr[2] ?: 'index';
        }
        return $action;
    }



}