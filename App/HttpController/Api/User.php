<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/6
 * Time: 16:51
 */
namespace App\HttpController\Api;

use App\common\constants\ErrorCode;
use App\common\service\token\UserAccessTokenService;
use EasySwoole\EasySwoole\Config;

class User extends ApiBase{

    protected $ignoreAuthAction = ['login'];

    function index()
    {

        $this->_success(['uid'=>$this->uid]);
    }

    function login()
    {
        $uid = $this->request()->getRequestParam('uid');

        if(!$uid){

            return $this->_error(ErrorCode::INVALID_CLIENT_PARAM,'uid is required');

        }

        $test = Config::getInstance()->getConf('TEST');

        $service = new UserAccessTokenService();

        $token = $service->generateAccessToken($uid);

        $this->_success(['token'=>$token,'action'=>$this->getAction(),'test'=>$test]);

    }
}