<?php
namespace App\HttpController;

use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\HttpAnnotation\AnnotationController;
use EasySwoole\I18N\I18N;

class BaseController extends AnnotationController
{



    /**
     * 获取用户的真实IP
     * @param string $headerName 代理服务器传递的标头名称
     * @return string
     */
    protected function clientRealIP($headerName = 'x-real-ip')
    {
        $server = ServerManager::getInstance()->getSwooleServer();
        $client = $server->getClientInfo($this->request()->getSwooleRequest()->fd);
        $clientAddress = $client['remote_ip'];
        $xri = $this->request()->getHeader($headerName);
        $xff = $this->request()->getHeader('x-forwarded-for');
        if ($clientAddress === '127.0.0.1') {
            if (!empty($xri)) {  // 如果有xri 则判定为前端有NGINX等代理
                $clientAddress = $xri[0];
            } elseif (!empty($xff)) {  // 如果不存在xri 则继续判断xff
                $list = explode(',', $xff[0]);
                if (isset($list[0])) $clientAddress = $list[0];
            }
        }
        return $clientAddress;
    }

    protected function input($name, $default = null) {
        $value = $this->request()->getRequestParam($name);
        return $value ?? $default;
    }

    protected function writeJson($errorCode=0, $msg = null,$error = "",$result = null)
    {
        if (!$this->response()->isEndResponse()) {
            $data =[
                'meta' => [
                    'code' => (int)$errorCode, //错误码
                    'msg' => (string)$msg, //用户端显示的错误信息
                    'error' => (string)$error, //开发者查看的错误信息
                    'request_uri' => $this->request()->getUri()->getPath(), //请求路径
                    'language' => $this->request()->getHeader('accept-language'),
                    'cip' => $this->clientRealIP()
                ],
                'response' => $result ? $result : new \stdClass() //响应信息主体
            ];
            $this->response()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type', 'application/json;charset=utf-8');
            $this->response()->withStatus(200);
            return true;
        } else {
            return false;
        }
    }

    /**
     * API成功返回
     * @param array $response
     * @param null $code
     * @throws \EasySwoole\I18N\Exception\Exception
     */
    protected function _success($response = [], $code = null)
    {
        if ($code) {
            $msg = I18N::getInstance()->translate($code);
        } else {
            $msg = '操作成功';
        }
        $this->writeJson(0,$msg,null,$response);
    }

    /**
     * API失败返回
     * @param $errorCode
     * @param null $error
     * @param array $response
     * @throws \EasySwoole\I18N\Exception\Exception
     */
    protected function _error($errorCode, $error = null, $response = [])
    {
        $msg = I18N::getInstance()->translate($errorCode);
        if (empty($msg)) {
            $msg = $error;
        }
        $this->writeJson(1, $msg, $error, $response);
    }
}