<?php

namespace App\Model;


use EasySwoole\EasySwoole\Logger;
use EasySwoole\ORM\AbstractModel;


class BaseModel extends AbstractModel
{

    protected $primaryKey = 'id';

    protected $tableName = '';

    public function getOne($id, $fields = '*')
    {
        $condition = [$this->primaryKey => $id];
        $one = self::getOneByCondition($condition, $fields);
        return $one;
    }

    /**
     *
     * @param $condition string|array
     * $condition 支持的格式：
     * 1.二位数组 [['id','1','>='],['id'=>3]]  //二维数组里面的数字支持2种方式。可联合使用
     * 2.字符串 "(id=2 and status=3) or id=3"
     * @param string|array $fields 查询的字段
     * @return BaseModel|array|bool|\EasySwoole\ORM\Db\Cursor|\EasySwoole\ORM\Db\CursorInterface|null
     * @throws \Throwable
     */
    public function getOneByCondition($condition=[], $fields = '*')
    {
        try {

            $model = $this->field($fields);

            if($condition){

                if (is_array($condition)) {

                    foreach ($condition as $item) {

                        list($field, $value, $option) = $item;

                        if ($field && $value && $option) {

                            $model->where($field, $value, $option);
                        } else {

                            $model->where($item);
                        }
                    }
                } else {

                    $model->where($condition);
                }
            }



            $one = $model->get();

        } catch (\Exception $e) {
            Logger::getInstance()->error((string)$e->getMessage(), 'getone.error');
            return false;
        }
        return $one;
    }

    /**
     * @param $condition
     * $condition 支持的格式：
     * 1.二位数组 [['id','1','>='],['id'=>3]]  //二维数组里面的数字支持2种方式。可联合使用
     * 2.字符串 "(id=2 and status=3) or id=3"
     * @param string|array $fields 查询的字段
     * @param array $extra
     * $extra['page'] 表示第几页
     * $extra['limit'] 表示每页条数
     * $extra['order_by']  支持3中类型
     * 1-string :"id" 表示按id降序。注意不需要desc 字符串比较局限不建议使用,
     * 2-array:["id","desc"] 表示按id降序
     * 3-list:[["id","desc"],["sort",'asc']] 表示先按id降序。再按sort升序
     * @return array|bool|\EasySwoole\ORM\Collection\Collection|\EasySwoole\ORM\Db\Cursor|\EasySwoole\ORM\Db\CursorInterface
     * @throws \Throwable
     */
    public function getList($condition=[], $fields = "*", $extra = [])
    {
        try {

            $query = $this->field($fields);

            if ($condition) {

                if (is_array($condition)) {

                    foreach ($condition as $item) {

                        list($field, $value, $option) = $item;

                        if ($field && $value && $option) {

                            $query->where($field, $value, $option);
                        } else {

                            $query->where($item);
                        }
                    }
                } else {

                    $query->where($condition);
                }

            }

            if ($extra['limit'] > 0 && $extra['page'] >= 1) { //分页条目数
                $query->limit(($extra['page'] - 1) * $extra['limit'], $extra['limit']);
            }

            if (isset($extra['group_by'])) {

                $query->group($extra['group_by']);
            }

            if ($extra['order_by']) {

                if (is_array($extra['order_by'])) {

                    foreach ($extra['order_by'] as $item) {

                        if (is_array($item)) {

                            list($field, $order) = $item;

                            $query->order($field, $order);


                        } else {

                            $query->order($item);
                            break;
                        }
                    }
                } else {

                    $query->order($extra['order_by']);
                }


            }

            $list = $query->all();

        } catch (\Exception $e) {
            Logger::getInstance()->error((string)$e->getMessage(), 'list.error');
            return false;
        }

        return $list;
    }

    /**
     * 插入
     * @param $data
     * @return bool|int
     * @throws \Throwable
     */
    public function mInsert($data)
    {
        try {

            $model = self::create($data);
            $id = $model->save();

        } catch (\Exception $e) {
            Logger::getInstance()->error((string)$e->getMessage(), 'insert.error');
            return false;
        }
        return $id;
    }

    public function mUpdate($id, $data)
    {
        try {

            $res = self::create()->update($data, [$this->primaryKey => $id]);

        } catch (\Exception $e) {
            Logger::getInstance()->error((string)$e->getMessage(), 'update.error');
            return false;
        }
        return $res;

    }

    /**
     * @param array $condition
     * @param null $field
     * @return int|null
     */
    public function count($condition = [],$field = null)
    {
        $query = $this;
        if ($condition) {

            if (is_array($condition)) {

                foreach ($condition as $item) {

                    list($field, $value, $option) = $item;

                    if ($field && $value && $option) {

                        $query->where($field, $value, $option);
                    } else {

                        $query->where($item);
                    }
                }
            } else {

                $query->where($condition);
            }

        }

        return $query->lastQueryResult()->getTotalCount();
    }

    /**
     * @param $data //选哟更新的数据数组
     * @param $condition //条件 例如 ['id'=>5] 表示更新id=5的记录
     * @return bool
     * @throws \Throwable
     */
    public function updateByCondition($data,$condition)
    {
        try {

            $res = self::create()->update($data, $condition);

        } catch (\Exception $e) {
            Logger::getInstance()->error((string)$e->getMessage(), 'updae.condition.error');
            return false;
        }
        return $res;
    }


}

