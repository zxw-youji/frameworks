<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/4
 * Time: 13:47
 */
namespace App\ProcessReload;

use EasySwoole\Component\Process\AbstractProcess;
use EasySwoole\EasySwoole\Logger;

class Process extends AbstractProcess
{

    protected function run($arg)
    {
        $pid = getmypid();
        Logger::getInstance()->info("process for pid {$pid} start");
        Work::run();
    }

    function onShutDown()
    {
        $pid = getmypid();
        Logger::getInstance()->info("process for pid {$pid} shutdown");
        parent::onShutDown();
    }
}