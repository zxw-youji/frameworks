<?php
namespace App\common\message\us;


use App\common\constants\ErrorCode;

class US extends ErrorCode{

    const SUCCESS = 'success';
    const INVALID_CLIENT_PARAM = 'params error';
    const NOT_AUTH = 'please login!';


}