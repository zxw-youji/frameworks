<?php
namespace App\common\message\cn;


use App\common\constants\ErrorCode;

class CN extends  ErrorCode {

    const SUCCESS = '成功';
    const INVALID_CLIENT_PARAM = '参数错误';
    const NOT_AUTH = '请登录';

}