<?php

namespace App\common\constants;

use EasySwoole\I18N\AbstractDictionary;
/**
 * 错误码常量
 */
class ErrorCode extends AbstractDictionary
{
    const SUCCESS = 'SUCCESS';

    const INVALID_CLIENT_PARAM = 'INVALID_CLIENT_PARAM';
    const NOT_FOUND = 'NOT_FOUND';
    const NOT_ALLOWED_METHOD = 405;
    const SYSTEM_ERROR = 500;
    const NOT_ACCEPTABLE = 406;
    const NOT_AUTH = 'NOT_AUTH';

}