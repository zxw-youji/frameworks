<?php
namespace App\common\service\token;



use EasySwoole\Jwt\Jwt;

/**
 * 授权jwt服务
 * @author zxw
 */
abstract class AccessTokenService
{


    protected $secretKey = 'jo)I3#$g2';
    protected $tokenTime = 7 * 86400;


    /**
     * 生成Access-Token
     * @param int $uid
     * @return string|bool
     */
    public function generateAccessToken($uid)
    {
        $jwt = Jwt::getInstance()->setSecretKey($this->secretKey)->publish();
        $jwt->setAlg('HMACSHA256');
        $jwt->setJti($uid);
        $token = $jwt->__toString();
        return $token ? $token : false;
    }

    /**
     * 验证accessToken
     * @param $accessToken
     * @return bool|int
     */
    public function checkAccessToken($accessToken)
    {
        try {
            $jwtObject = Jwt::getInstance()->setSecretKey($this->secretKey)->decode($accessToken);

            $status = $jwtObject->getStatus();

            if($status==1){
                return $jwtObject->getJti();
            }else{

                return false;
            }
        } catch (\EasySwoole\Jwt\Exception $e) {
            return false;
        }
    }



}