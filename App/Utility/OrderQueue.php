<?php
namespace App\Utility;

use EasySwoole\Component\Singleton;
use EasySwoole\Queue\Queue;

class OrderQueue extends Queue
{
    use Singleton;
}