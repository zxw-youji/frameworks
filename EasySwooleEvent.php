<?php
namespace EasySwoole\EasySwoole;


use App\common\message\cn\CN;
use App\common\message\us\US;
use App\Process\TestProcess;
use App\Utility\MyQueue;
use App\Utility\OrderQueue;
use App\Utility\QueueProcess;
use EasySwoole\Component\Process\Manager;
use EasySwoole\Component\Timer;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\I18N\I18N;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\DbManager;
use EasySwoole\Queue\Driver\Redis;
use EasySwoole\Queue\Job;
use EasySwoole\Redis\Config\RedisConfig;
use EasySwoole\RedisPool\RedisPool;
use EasySwoole\Utility\Time;


class EasySwooleEvent implements Event
{
    protected $languageList = ['CN','US'];


    public static function initialize()
    {
        // TODO: Implement initialize() method.
        date_default_timezone_set('Asia/Shanghai');
        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        $config->setMaxObjectNum(20);//配置连接池最大数量
        DbManager::getInstance()->addConnection(new Connection($config));
    }

    public static function mainServerCreate(EventRegister $register)
    {

        $config = new RedisConfig([
            'host'=>'127.0.0.1',
            'port'      => '6379',
            'db'        => null
        ]);

        $redis = new RedisPool($config);
        $driver = new Redis($redis);
        MyQueue::getInstance($driver);
        OrderQueue::getInstance($driver);
        //注册一个消费进程
        Manager::getInstance()->addProcess(new QueueProcess());
        //模拟生产者，可以在任意位置投递
//        $register->add($register::onWorkerStart,function ($ser,$id){
//            if($id == 0){
//                Timer::getInstance()->loop(3000,function (){
//                    $job = new Job();
//                    $job->setJobData(['time'=>\time()]);
//                    MyQueue::getInstance()->producer()->push($job);
//                });
//            }
//        });
        //注册语言包
        I18N::getInstance()->addLanguage(new CN(),'CN');
        I18N::getInstance()->addLanguage(new US(),'US');
        //设置默认语言包
        I18N::getInstance()->setDefaultLanguage('US');

    }

    public static function onRequest(Request $request, Response $response): bool
    {
        // TODO: Implement onRequest() method.
        return true;
    }

    public static function afterRequest(Request $request, Response $response): void
    {
        // TODO: Implement afterAction() method.
    }

    public static function setLanguage()
    {



    }
}